/**
 * Created by root on 27/10/16.
 */
$(document).ready(function() {
    $( function() {
        $( "#voteDialog" ).dialog({autoOpen: false,modal: true, width:'50%'});
        $( "#resultsDialog" ).dialog({autoOpen: false,modal: true, width:'50%'});
    } );
});


function showPopup(controller, repoName, repoUrl, popupId){
    $.ajax({url: controller,
        data: {
            repoName:   repoName,
            repoUrl:    repoUrl
        },
        success: function(result){
            $("#"+popupId).html(result);
            $("#"+popupId).dialog( "open" );
        }});
}

function showResultsPopup(controller, repoName, repoUrl) {
    showPopup(controller, repoName, repoUrl, 'resultsDialog');
}

function showVotePopup(controller, repoName, repoUrl) {
    showPopup(controller, repoName, repoUrl, 'voteDialog');
}