<g:form name="formVote">

    <g:if test="${flash.message}">
        <div class="alert ${flashClass}">
            ${flash.message}
        </div>
    </g:if>

    <g:hiddenField name="repoName" value="${params.repoName}" />
    <g:hiddenField name="repoUrl" value="${params.repoUrl}" />

    <div class="form-group text-center">
        <h2>Vote for ${params.repoName}</h2>
    </div>

    <div class="form-group mandatory">
        <label for="voteOption">Vote option</label>
        <select class="form-control" id="voteOption" name="voteOption">
            <option>Like</option>
            <option>Dislike</option>
        </select>
    </div>

    <div class="form-group mandatory">
        <label for="comment">Comment</label>
        <textarea class="form-control" id="comment" name="comment" rows="3"></textarea>
    </div>

    <div class="form-group text-center">
        <g:submitToRemote class=""
                         action="doVote" update="voteDialog" value="Let's vote!" />
    </div>
</g:form>