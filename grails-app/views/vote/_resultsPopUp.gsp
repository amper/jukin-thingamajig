<g:if test="${votes.isEmpty()}">
    <div class="alert alert-warning">
        No results have been found for ${params.repoName}.
    </div>
</g:if>
<g:else>
    <div class="form-group text-center">
        <h1>Results for ${params.repoName}</h1>
    </div>

    <div class="table-responsive">
        <table class="table">
            <tbody>
            <g:each in="${votes}" var="vote">
                <tr>
                    <td class="newline-word">${vote.comment}</td>
                    <td class="text-center">
                        ${vote.voteValue}
                    </td>
                </tr>
            </g:each>
            </tbody>
        </table>
    </div>
</g:else>