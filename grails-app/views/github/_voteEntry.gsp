<div class="row rowstripped tiny-space">

    <div class="col-lg-8">
        <h1>${bestRepo.full_name}</h1><br/><br/>
        ${bestRepo.description}<br/><br/>
        <a href="${bestRepo.url}">${bestRepo.html_url}</a><br/><br/>
        Latest commit: ${bestRepo.lastCommit}<br/>
    </div>

    <div class="col-md-6 col-lg-4 separated-from-top">
            <div class="text-center pull-left" >
                <button type="button" class="btn btn-default" onClick="showVotePopup('${createLink(controller:'vote',action: 'showVotePopup')}','${bestRepo.full_name}','${bestRepo.url}');">VOTE</button>
            </div>
            <div class="text-center ">
                <button type="button" class="btn btn-default" onClick="showResultsPopup('${createLink(controller:'vote',action: 'showResultsPopup')}','${bestRepo.full_name}','${bestRepo.url}');">RESULTS</button>
            </div>
    </div>
</div>