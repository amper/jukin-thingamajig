<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="mainApp"/>
		<title>Github TOPRated</title>
	</head>
	<body>
		<div class="container">

			<g:each in="${bestRepositories}" >
				<g:render template="voteEntry" model="['bestRepo':it]"/>
			</g:each>

		</div>

		<!-- MODALS POPUPS-->
		<div id="voteDialog" title="Vote"></div>
		<div id="resultsDialog" title="Results"></div>

	</body>
</html>