<!DOCTYPE html>
<head>
    <title>Github TOPRated</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="${resource(dir: 'images', file: 'favicon.ico')}" type="image/x-icon">
    <link rel="apple-touch-icon" href="${resource(dir: 'images', file: 'apple-touch-icon.png')}">
    <link rel="apple-touch-icon" sizes="114x114" href="${resource(dir: 'images', file: 'apple-touch-icon-retina.png')}">


    <link rel="stylesheet" href="${resource(dir: 'css', file: 'mobile.css')}" type="text/css">
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'mainApp.css')}" type="text/css">
    <link rel="stylesheet" href="${resource(dir: 'assets/bootstrap/css', file: 'bootstrap.min.css')}" type="text/css">

    <g:layoutHead/>
    <g:javascript library="application"/>
    <link rel="javascript" href="${resource(dir: 'assets/bootstrap/js', file: 'bootstrap.min.js')}" type="text/js">
    <link rel="javascript" href="${resource(dir: 'js', file: 'mainApp.js')}" type="text/js">


    <!-- ASSETS REPOSITORIES -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <r:layoutResources />
</head>
<body>
<div class="container header">
        <div class="row">
            <a href="http://www.jukinvideo.com" class="col-lg-4 visible-lg">
                <img width="150" src="${resource(dir: 'images', file: 'JukinVideo_Logo042314.png')}" alt="Jukin"/>
            </a>
            <h1 class="col-md-12 col-lg-4 text-center">TOP RATED REPOSITORIES</h1>
            <a href="http://www.github.com" class="col-lg-4 text-right visible-lg">
                <img width="250" src="${resource(dir: 'images', file: 'mark-github.png')}" alt="github"/>
            </a>
        </div>
    </div>

    <g:layoutBody/>

    <div class="footer" role="contentinfo"></div>

    <r:layoutResources />
</body>
</html>
