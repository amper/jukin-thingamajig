package jukin.thingamajig

import exceptions.BusinessException
import grails.converters.JSON
import grails.plugins.rest.client.RestBuilder
import grails.transaction.Transactional
import org.codehaus.groovy.grails.commons.ConfigurationHolder

@Transactional
class VoteService {
    def grailsApplication

    def serviceMethod() {

    }

    def doVote(repoName, repoUrlselected, voteOption, engineerComment) {
        RestBuilder rest = new RestBuilder()

        def response = rest.post(grailsApplication.config.grails.serverURL + "/votes") {
            header 'Content-Type', 'application/json'
            accept JSON
            json {
                name = repoName
                repoUrl = repoUrlselected
                voteValue = voteOption
                comment = engineerComment
            }
        }

        if(!response.status.equals(201)){
            throw new BusinessException(response.getText())
        }

    }
}
