class UrlMappings {

	static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }

        "/" ( controller:'github', action:'list', param:"repos" )
        "500"(view:'/error')

        "/votes" (resources: "vote")
        "/votes/$id" (resources: "vote")
	}
}
