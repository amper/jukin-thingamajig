package jukin.thingamajig

class Vote {
    String name
    String repoUrl
    String voteValue
    String comment

    static mapping = {
        comment type: 'text'
    }

    static constraints = {
        name nullable: false
        repoUrl nullable: false
        voteValue nullable: false
        comment nullable: false
    }

    public boolean validate(){
          if (!this.name || this.name.equals("")){
               this.errors.reject("Name of the repository is mandatory")
           }

           if (!this.repoUrl || this.repoUrl.equals("")){
               this.errors.reject("URL of the repository is mandatory")
           }

           if (!this.voteValue || this.voteValue.equals("")){
               this.errors.reject("Vote option is mandatory")

           }

           if (!this.comment || this.comment.equals("")){
               this.errors.reject("Comment is mandatory")
           }

        return !this.hasErrors()
    }

    public String getValidationErrors(){
        def validationErrors = new StringBuffer()

        this.errors?.each {
            validationErrors.append (it.globalError?.getCode()+ " - ")
        }

        return validationErrors.toString()
    }

    public String toString(){
        return "Vote Name: "  + this.name
            +  "Repo URL: "   + this.repoUrl
            +  "Vote Value: " + this.voteValue
            +  "Comment: "    + this.comment;
    }

}
