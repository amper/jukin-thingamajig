package jukin.thingamajig

import exceptions.BusinessException
import grails.converters.JSON
import grails.plugins.rest.client.RestBuilder
import grails.rest.RestfulController

class VoteController extends AbstractVoteRestController {

    def voteService

    def index() {
        redirect(action: "list")
    }

    def showVotePopup(){
        flash.message = ""
        render(template:"votePopUp", model:['repoUrl':params.repoUrl, 'repoName':params.repoName])
    }

    def showResultsPopup(){
        flash.message = ""

        def votes = Vote.findAllByName(params.repoName)

        render(template:"resultsPopUp", model:['repoUrl':params.repoUrl, 'repoName':params.repoName, votes:votes])
    }

    def doVote(){
        def flashClass = 'alert-success'

        try{
            voteService.doVote(params.repoName, params.repoUrl, params.voteOption, params.comment)
            flash.message = "Your vote has been processed."
        }catch (BusinessException e){
            flashClass = 'alert-danger'
            flash.message = e.getMessage()
        }

        render(template:"votePopUp", model:['repoUrl':params.repoUrl, 'repoName':params.repoName, 'flashClass':flashClass])
    }

}
