package jukin.thingamajig

import grails.rest.RestfulController

class AbstractVoteRestController extends RestfulController{
    static allowedMethods = ['GET', 'PUT', 'POST']
    static responseFormats = ['json']

    AbstractVoteRestController() {
        super(Vote)
    }

    @Override
    def list(){
        // GET
        respond Vote.list()
    }

    @Override
    def save(){
        // POST
        Vote vote = new Vote(request.getJSON())

        if(!vote.validate()){
            render (status:504, text:vote.getValidationErrors())
            return
        }

        vote.save(failOnError:true, flush:true, insert: true)
        render (status:201) // OK!
    }

    @Override
    def delete(){
        response.sendError 405
    }

    @Override
    def update(Vote vote){
        // PUT
        vote?.save flush:true

        respond vote
    }

}
